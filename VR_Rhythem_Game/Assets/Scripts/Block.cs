using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Red
}

//GameObject that reference the game model(class type)
//Float, bool, int hold a vaule. Hold data
//Varble store a vaule or reference something
//function = mentions
//void don't give back feedback/reture a vaule
public class Block : MonoBehaviour
{
   public BlockColor color;
   public GameObject brokenBlockLeft;
   public GameObject brokenBlockRight;
   public float brokenBlockForce;
   public float brokenBlockTorque;
   public float brokenBlockDestroyDelay;
   
   
   void OnTriggerEnter (Collider other)
   {
    if(other.CompareTag("Red_sword"))
    {
        if(color == BlockColor.Red &&  GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityTreshold)
        {
            
            GameManager.instance.AddScore();
        }
        else
        {
            GameManager.instance.HitWrongBlock();
        }
        
        Hit();
    }
    else if(other.CompareTag("Green_sword"))
    {
        if(color == BlockColor.Green &&  GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityTreshold)
        {
            GameManager.instance.AddScore();
        }
        else
        {
            GameManager.instance.HitWrongBlock();
        }
        
        Hit();
    }
   
   }
   
   void Hit ()
   {
    //enable the broken piece
    brokenBlockLeft.SetActive(true);
    brokenBlockRight.SetActive(true);
    
    //remove them as childern
    brokenBlockLeft.transform.parent = null;
    brokenBlockRight.transform.parent = null;
    
    //add force to them
    Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
    Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
    
    leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
    rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
    
    //add Torque
    leftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
    rightRig.AddTorque(transform.forward * brokenBlockTorque, ForceMode.Impulse);
    
    // destory the broken piece after a few second
    Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
    Destroy(brokenBlockRight, brokenBlockDestroyDelay);
    
    //Destory the main block
    Destroy(gameObject);
    
    
   }
}
